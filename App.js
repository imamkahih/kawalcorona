import { StatusBar } from 'expo-status-bar';
import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Application from './src/index'

// Import the functions you need from the SDKs you need
import firebase from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBVkTCV6-LyyuPiTA1JIcVvoK-_girWYwE",
  authDomain: "kawalcorona-a31cb.firebaseapp.com",
  projectId: "kawalcorona-a31cb",
  storageBucket: "kawalcorona-a31cb.appspot.com",
  messagingSenderId: "116362517851",
  appId: "1:116362517851:web:455f3bd8916d2e4025ce7e"
};

// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default function App() {
  return (
    <Application/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
