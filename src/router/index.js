import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { NavigationContainer } from '@react-navigation/native';
import Landing from '../pages/Landing';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Home from '../pages/Home';
import Detail from '../pages/Detail';
import Profil from '../pages/Profil';

const Stack = createNativeStackNavigator();

export default function Router() {
  return (
    <Stack.Navigator>
      <Stack.Screen component={Landing} name="Landing" options={{ headerShown:false }}/>
      <Stack.Screen component={Login} name="Login"/>
      <Stack.Screen component={Register} name="Register"/>
      <Stack.Screen component={Home} name="Home" options={{ headerShown:false }}/>
      <Stack.Screen component={Detail} name="Detail"/>
      <Stack.Screen component={Profil} name="Profil"/>
    </Stack.Navigator>
  )
}