import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, Button, Image} from 'react-native'
import * as firebase from 'firebase';
import axios from "axios";

export default function Profil({navigation}) {
  const [user, setUser] = useState({})

  useEffect(() => {
    const userInfo =firebase.auth().currentUser
    setUser(userInfo)
  }, [])

  const onLogout=()=>{
    firebase.auth()
      .signOut()
      .then(()=>{
        console.log('user Sign out');
        navigation.navigate('Landing')
      }
    )
  }
  return (
    <View>
      <Text style={styles.textHeader}>Profil</Text>
      <Image source={require('../assets/p.jpg')} style={styles.imageHeader}/>
      <Text style={styles.email}>{user.email}</Text>
      <Button onPress={onLogout} title="Logout"/>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    padding:20
  },
  textHeader:{
    fontSize:28,
    fontWeight:"bold",
    marginTop:50,
    alignSelf:"center"
  },
  imageHeader:{
    alignSelf:'center',
    marginTop:20,
    resizeMode:'stretch',
    height:200,
    width:200,
  },
  email:{
    fontSize:24,
    alignSelf:'center',
    marginBottom:10,
  }
})
