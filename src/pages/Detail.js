import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import * as firebase from 'firebase';

export default function Detail(navigation) {
  const [user, setUser] = useState({})

  useEffect(() => {
    const userInfo =firebase.auth().currentUser
    setUser(userInfo)
  }, [])

  return (
    <View style={styles.container}>
      <Text style={styles.textHeader}>Detail COVID-19 Indonesia</Text>
      <Text style={styles.textDesc}>Positif : 4,220,206</Text>
      <Text style={styles.textDesc}>Sembuh : 4,046,891</Text>
      <Text style={styles.textDesc}>Meninggal : 142,261</Text>
      <Text style={styles.textDesc}>Sedang Dirawat : 31,054</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'flex-start',
    padding:20
  },
  textHeader:{
    fontSize:24,
    fontWeight:"bold",
    marginTop:70
  },
  textDesc:{
    fontSize:20,
    marginTop:20,
  },
})
