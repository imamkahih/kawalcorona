import React from 'react'
import { Image, StyleSheet, Text, View, Button } from 'react-native'

export default function Landing({navigation}) {
  return (
    <View style={styles.container}>
      <Text style={styles.textHeader}>Kawal Corona</Text>
      <Image source={require('../assets/corona.jpeg')} style={styles.imageHeader}/>
      <Text style={styles.textDesc}>COVID-19 (coronavirus disease 2019) adalah jenis penyakit baru yang disebabkan oleh virus dari golongan coronavirus, yaitu SARS-CoV-2 yang juga sering disebut virus Corona. Tujuan dibuatnya aplikasi ini adalah mengtahui tentang update data tentang penyakit COVID-19 baik itu didunia maupun di Indonesia.</Text>
      <View style={styles.choice}>
        <Text style={styles.textHint}>Sudah mempunyai akun? silahkan klik login.</Text>
        <Button title="Login" onPress={()=>navigation.navigate("Login")}/>
        <Text style={styles.textHint}>Belum mempunyai akun? silahkan klik Register.</Text>
        <Button title="Register" onPress={()=>navigation.navigate("Register")} color="deepskyblue"/>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'flex-start',
    padding:20
  },
  textHeader:{
    fontSize:28,
    fontWeight:"bold",
    marginTop:70
  },
  imageHeader:{
    marginTop:20,
  },
  textDesc:{
    fontSize:16,
    marginTop:20,
    textAlign:"justify"
  },
  textHint:{
    fontSize:16,
    textAlign:"left",
    marginTop:30
  },
  choice:{
    flex:1,
    marginTop:45
  }
})