import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, Button, FlatList} from 'react-native'
import * as firebase from 'firebase';

export default function Home({navigation}) {
  const [user, setUser] = useState({})

  useEffect(() => {
    const userInfo =firebase.auth().currentUser
    setUser(userInfo)
  }, [])

  const onLogout=()=>{
    firebase.auth()
      .signOut()
      .then(()=>{
        console.log('user Sign out');
        navigation.navigate('Landing')
      }
    )
  }

  return (
    <View style={styles.container}>
      <Text style={styles.textHeader}>Data COVID-19 Indonesia</Text>
      <Text style={styles.textDesc}>Positif : 4,220,206</Text>
      <Button title="Lihat Detail" onPress={()=>navigation.navigate("Detail")} />
      <View style={styles.choice}>
        <Button title="Lihat Profil" onPress={()=>navigation.navigate("Profil")} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'flex-start',
    padding:20
  },
  textHeader:{
    fontSize:28,
    fontWeight:"bold",
    marginTop:70
  },
  textDesc:{
    fontSize:24,
    marginTop:20,
    marginBottom:20,
  },
  choice:{
    flex:1,
    marginTop:450
  }
})
