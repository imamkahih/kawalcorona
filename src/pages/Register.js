import React, {useState, useEffect} from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';

export default function Register({navigation}) {
  const firebaseConfig = {
    apiKey: "AIzaSyBVkTCV6-LyyuPiTA1JIcVvoK-_girWYwE",
    authDomain: "kawalcorona-a31cb.firebaseapp.com",
    projectId: "kawalcorona-a31cb",
    storageBucket: "kawalcorona-a31cb.appspot.com",
    messagingSenderId: "116362517851",
    appId: "1:116362517851:web:455f3bd8916d2e4025ce7e"
  };

  if(!firebase.apps.length){
    firebase.initializaApp(firebaseConfig)
  }

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const submit=()=>{
    const data = {
      email,
      password
    }
    console.log(data)
    firebase.auth().createUserWithEmailAndPassword(email, password).then(()=>{
      console.log('Register Berhasil');
      navigation.navigate("Home");
    }).catch(()=>{
      console.log("Register gagal")
    })
  }

  return (
    <View style={styles.container}>
      <Text style={styles.textHeader}>Register</Text>
      <TextInput
        style={styles.input}
        placeholder="Email"
        value={email}
        onChangeText={(value)=>setEmail(value)}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        value={password}
        onChangeText={(value)=>setPassword(value)}
      />
      <View style={styles.choice}>
        <Button onPress={submit} title="REGISTER"/>
        <Text style={styles.textHint}>Sudah mempunyai akun? silahkan klik login.</Text>
        <Button title="Login" onPress={()=>navigation.navigate("Login")} color="deepskyblue"/>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems:'center',
    justifyContent:'flex-start'
  },
  input:{
    borderWidth:1,
    borderColor:'grey',
    paddingHorizontal:10,
    paddingVertical: 10,
    width: 300,
    marginBottom: 10,
    borderRadius: 6,
    marginTop: 10
  },
  textHint:{
    fontSize:16,
    textAlign:"left",
    marginTop:30
  },
  textHeader:{
    fontSize:28,
    fontWeight:"bold",
    marginTop:70
  },
  choice:{
    flex:1,
    marginTop:5,
    paddingLeft:30,
    paddingRight:30
  }
})
