import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router/index';

export default function index() {
    return (
        <NavigationContainer>
            <Router />
        </NavigationContainer>
    )
}
